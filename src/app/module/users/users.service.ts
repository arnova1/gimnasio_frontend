import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUsers } from './users';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http:HttpClient) { }

  public query(): Observable<IUsers[]>{
    return this.http.get<IUsers[]>(`${environment.END_POINT}/api/users`)
    .pipe(map(res=>{
      return res;
    }))
  }
}
