export interface IUsers {

    idUsers?: number;
    name?: string;
    lastName?: string;
    documentNumber?: string;
    email?: string;
    phone?: string;
}

export class Users implements IUsers{
    idUsers?: number;
    name?: string;
    lastName?: string;
    documentNumber?: string;
    email?: string;
    phone?: string;
}
