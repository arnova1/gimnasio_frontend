import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaUpdateComponent } from './ficha-update.component';

describe('FichaUpdateComponent', () => {
  let component: FichaUpdateComponent;
  let fixture: ComponentFixture<FichaUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
