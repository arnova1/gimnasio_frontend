import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaListComponent } from './ficha-list.component';

describe('FichaListComponent', () => {
  let component: FichaListComponent;
  let fixture: ComponentFixture<FichaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
