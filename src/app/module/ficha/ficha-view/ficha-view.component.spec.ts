import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaViewComponent } from './ficha-view.component';

describe('FichaViewComponent', () => {
  let component: FichaViewComponent;
  let fixture: ComponentFixture<FichaViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
