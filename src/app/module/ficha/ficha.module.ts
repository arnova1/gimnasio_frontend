import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FichaRoutingModule } from './ficha-routing.module';
import { FichaListComponent } from './ficha-list/ficha-list.component';
import { FichaCreateComponent } from './ficha-create/ficha-create.component';
import { FichaUpdateComponent } from './ficha-update/ficha-update.component';
import { FichaViewComponent } from './ficha-view/ficha-view.component';


@NgModule({
  declarations: [FichaListComponent, FichaCreateComponent, FichaUpdateComponent, FichaViewComponent],
  imports: [
    CommonModule,
    FichaRoutingModule
  ]
})
export class FichaModule { }
