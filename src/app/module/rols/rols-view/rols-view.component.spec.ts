import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolsViewComponent } from './rols-view.component';

describe('RolsViewComponent', () => {
  let component: RolsViewComponent;
  let fixture: ComponentFixture<RolsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
