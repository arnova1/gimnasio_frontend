import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolsUpdateComponent } from './rols-update.component';

describe('RolsUpdateComponent', () => {
  let component: RolsUpdateComponent;
  let fixture: ComponentFixture<RolsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
