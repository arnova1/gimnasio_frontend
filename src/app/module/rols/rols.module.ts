import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolsRoutingModule } from './rols-routing.module';
import { RolsListComponent } from './rols-list/rols-list.component';
import { RolsCreateComponent } from './rols-create/rols-create.component';
import { RolsUpdateComponent } from './rols-update/rols-update.component';
import { RolsViewComponent } from './rols-view/rols-view.component';


@NgModule({
  declarations: [RolsListComponent, RolsCreateComponent, RolsUpdateComponent, RolsViewComponent],
  imports: [
    CommonModule,
    RolsRoutingModule
  ]
})
export class RolsModule { }
