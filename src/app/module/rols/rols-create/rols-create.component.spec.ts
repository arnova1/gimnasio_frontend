import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolsCreateComponent } from './rols-create.component';

describe('RolsCreateComponent', () => {
  let component: RolsCreateComponent;
  let fixture: ComponentFixture<RolsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
