import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path:'ficha',
    loadChildren: ()=> import('./module/ficha/ficha.module')
    .then(m=> m.FichaModule)
  },
  {
    path: 'rols',
    loadChildren: ()=> import('./module/rols/rols.module')
    .then(m=> m.RolsModule)
  },
  {
    path: 'users',
    loadChildren: ()=> import('./module/users/users.module')
    .then(m=> m.UsersModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
